const express = require('express'),
      s3Handler = require('./handlers/s3Handler'),
      multiparty = require('multiparty'),
      fs = require('fs'),
      path = require('path'),
      moment = require("moment"),
      dbHandler = require('./handlers/dbHandler');

const router = express.Router();

router.post('/uploadFiles', uploadFiles);
router.get('/getAllImages',getAllImages);
router.post('/removeObject',removeObject);

module.exports = router;

async function getAllImages(req, res, next){
    try {
        const images = await dbHandler.getAllImages();
        res.send(images);
    }
    catch(err){
        res.send(err);
    }
}

async function uploadFiles(req, res, next) {
    const uploadsPath = path.resolve('./public/uploads');
    try {
        const newFiles = !req.body.files || req.body.files.length <= 1 ? await moveFiles(req, uploadsPath) : req.body.files;
        s3Handler.s3Upload(newFiles, data => {
            dbHandler.saveImages(newFiles).then((result) => {
                clearDir();
                res.send(data);
            })
        });
    }
    catch(err){
        res.send(err);
    }
}

async function removeObject(req, res, next) {
    try {
        const s3Removed = await dbHandler.removeObject(req.body.filename);
        res.send(`${req.body.filename} deleted`);
        const dbRemoved = await s3Handler.s3RemoveObject(req.body.filename);

    }
    catch(err) {
        res.send(err);
    }

}

function moveFiles(req, uploadsPath) {
    return new Promise((resolve,reject) => {
        let uploaded = [];
        let form = new multiparty.Form();
        form.parse(req, function (err, fields, files) {
            for (let i = 0; i < files.file.length; i++) {
                let file = files.file[i];
                const oldpath = file.path;
                const newpath = `${uploadsPath}\\${file.originalFilename}`;
                fs.rename(oldpath, newpath, (err) => {
                    if (err) reject(err);
                    uploaded.push({
                        name: file.originalFilename,
                        path: newpath,
                        publicUrl: `https://s3.us-east-2.amazonaws.com/nadavtest/Client_Images/${file.originalFilename}`,
                        uploadedAt: moment().format("DD/MM/YYYY HH:ss:mm"),
                        title: file.originalFilename.split('.')[0]
                    });
                    if (uploaded.length === files.file.length)
                        resolve(uploaded);
                });
            }
        });
    });
};

function clearDir() {
    const uploadsPath = path.resolve('./public/uploads');
    const files = fs.readdirSync(uploadsPath);
    if (files.length > 0){
        for (var i = 0; i < files.length; i++) {
            var filePath = uploadsPath + '/' + files[i];
            if (fs.statSync(filePath).isFile())
                fs.unlinkSync(filePath);
        }
    }
}