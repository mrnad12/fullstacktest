var app = angular.module("router", ["ngRoute",'ngMaterial','lfNgMdFileInput']);

app.config(config);

config.$inject = ['$routeProvider', '$locationProvider'];

function config($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "/home/home.html",
            controller: 'homeCtrl'
        })
        .when("/upload", {
            templateUrl: "/upload/upload.html",
            controller: 'uploadCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}