const express = require('express'),
      path = require('path');

const router = express.Router();

router.get('/',index);

module.exports = router;

function index(req,res){
    res.sendFile(path.resolve('./client/master.html'));
}