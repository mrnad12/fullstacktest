app.controller('uploadCtrl',function($scope, $http, $mdDialog) {

    $scope.hide = true;
    $scope.uploadFiles = function(){
        let formData = new FormData();
        for (let i = 0; i < $scope.files.length; i++){
            formData.append('file', $scope.files[i].lfFile);
        }
        $scope.hide = false;
        $http.post('/api/uploadFiles', formData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(results => {
            $scope.showUploadedSuccess(results.data);
            $scope.hide = true;
        });
    };

    $scope.showUploadedSuccess = function(results, ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/upload/uploadSuccessDialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {files: results}
        })
    };

    function DialogController($scope,files, $mdDialog) {
        $scope.files = files;

        $scope.close = function(){
            $mdDialog.hide();
        }
    }
});