const express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    home = require('./index'),
    api = require('./server/api'),
    path = require('path');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`app started on port ${PORT} `);
});

app.use(express.static(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, '/client')));

app.use('/', home);
app.use('/api', api);