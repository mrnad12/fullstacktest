const s3 = require('s3'),
      AWS = require('aws-sdk');

const awsS3Client = new AWS.S3({
    accessKeyId: "AKIAJ2K7D2HOIZIBQZNQ",
    secretAccessKey: "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",
    region: "us-east-2",
    signatureVersion: 'v4',
});

const client = s3.createClient({
    s3Client: awsS3Client
});

exports.s3Upload = (files, cb) => {
    let promises = [];
    for(let i = 0; i < files.length; i ++) {
    promises.push(
        new Promise((resolve, reject) => {
            let file = files[i];
            const params = {
                localFile: file.path,
                s3Params: {
                    ACL: "public-read",
                    Bucket: "nadavtest",
                    Key: `Client_Images/${file.name}`
                }
            };

            const uploader = client.uploadFile(params);

            uploader.on('error', (err) => {
                reject("unable to upload:", err.stack);
            });

            uploader.on('end', () => {
                resolve({
                    file: file.name,
                    result: true
                })
            });
        })
    )}

    Promise.all(promises).then(data => {
        cb(data);
    })
};

exports.s3RemoveObject = (file) => {
    return new Promise((resolve, reject) => {
        const params = {
            Bucket: 'nadavtest',
            Delete: {
                Objects: [
                    {
                        Key: `Client_Images/${file}`
                    }
                ],
            },
        };

        client.deleteObjects(params, (err, data)=> {
            if (err)
                reject(err);
            else
                resolve(data);
        });
    })
};