const MongoClient = require('mongodb').MongoClient;
const mongoUri = "mongodb://nadavTests:Password1@18.218.99.198:27017/?authSource=Tests";



const connection = closure => MongoClient.connect(mongoUri,(err,db) => {
    if(err) return console.log(err);
    closure(db.db("Tests"));
});


exports.saveImages = (files) => {
    return new Promise((resolve,reject) => {
        connection(db => {
            db.collection("TestCollection").insertMany(files)
                .then(result =>{
                    resolve(result);
                }).catch(err =>{
                    reject(err);
            })
        })
    })
};

exports.getAllImages = () => {
    return new Promise((resolve,reject) => {
        connection(db => {
            db.collection("TestCollection").find({}).toArray()
                .then(results =>{
                    resolve(results);
                }).catch(err =>{
                reject(err);
            })
        })
    })
};

exports.removeObject = (filename) => {
    return new Promise((resolve,reject) => {
        connection(db => {
            db.collection("TestCollection").deleteOne({name: filename})
                .then(results =>{
                    resolve(results);
                }).catch(err =>{
                reject(err);
            })
        })
    })
};