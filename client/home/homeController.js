app.controller('homeCtrl',function($scope,$http) {
    $scope.emptyItems = true;
    $scope.hide = true;
    $scope.init = function(){
        $scope.hide = false;
        $http.get('/api/getAllImages').then(data=>{
            $scope.hide = true;
            if(data.data.length > 0)
                $scope.images = data.data;
            else
                $scope.emptyItems = false;
        })
    }
});