# FullStackTest by Nadav Avisar

# api call:
# get array of files
# returns array of files after uploading

	file is a JSON object of this type: 
	file:{
		name: " someName.png",
		path:"some local path"
	}
	# upload

	$http.post('/api/uploadFiles', {files: files}, {})
		.then(results => {
				//some code
			})
		.catch(err -> {
				//some code
			});
	# get

	$http.get('/api/getAllImages')
		.then(results => {
				//some code
			})
		.catch(err -> {
				//some code
			});

	# remove

	$http.post('/api/removeObject', {filename:"image.png"})
		.then(results => {
				//some code
			})
		.catch(err -> {
				//some code
			});